import pandas as pd

class CsvDataCruncher:
    def __init__(self, file_path, chunk_size):
        self._file_path = file_path
        self._chunk_size = chunk_size

    def get_chunk_size(self):
        return self._chunk_size

    def get_file_path(self):
        return self._file_path
    
    def get_chunks(self):
        return pd.read_csv(self.get_file_path(), chunksize=self.get_chunk_size())

    def get_cell(self, column, row_number):
        df = self.get_row(row_number)
        return df[column]

    def get_row(self, row_number): 
        return self.crunch(self.filter_row, row_number)

    def get_column(self, column): 
        return self.crunch(self.filter_column, column)

    def query(self, expression): 
        return self.crunch(self.filter_by_query, expression)

    def filter_row(self, df, row_number):
        df2 = None
        if row_number in df.index:
            df2 = df.loc[row_number, :]
        return df2

    def filter_column(self, df, column):
        return df[column]

    def filter_by_query(self, df, expression):
        return df.query(expression)

    def crunch(self, method_to_run, *args):
        chunk_list = []  

        df_chunks = self.get_chunks()
        for df_chunk in df_chunks:  
            chunk_filter = method_to_run(df_chunk, *args)
            chunk_list.append(chunk_filter)

        return pd.concat(chunk_list)

 
dc = CsvDataCruncher('50000 Sales Records.csv', 10000)

print('\nTask 1:  Get specific cell')
print(dc.get_cell('Country', 17))

print('\nTask 2:  All values of column `Country`')
print(dc.get_column('Country'))

print('\nTask 3:  Get specific row')
print(dc.get_row(17))

print('\nTask 4:  All sales from Ukraine...')
print(dc.query("`Country` == 'Ukraine'"))
